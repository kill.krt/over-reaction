const path = require("path");
const fs = require("fs");

const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

const appDirectory = fs.realpathSync(process.cwd());

module.exports = {
    devtool: "source-map",
    entry: path.resolve(appDirectory, "src/app.ts"),  // Path to the main .ts file
    output: {
        filename: "js/overreaction.js",                    // Name for the javascript file that is created/compiled in memory
        path: path.resolve(__dirname, '../dist')              // Output folder (used also by Clean Webpack Plugin)
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"],
    },
    devServer: {                                           // Temporary, it will be replaced by rocket server
        host: "0.0.0.0",
        port: 8080,
        disableHostCheck: true,
        contentBase: path.resolve(appDirectory, "public"), // Tells webpack to serve from the public folder
        publicPath: "/",
        hot: true,
    },
    module: {
        rules: [
            // Typescript
            {
                test: /\.tsx?$/,
                use: "ts-loader",
                exclude: /node_modules/,
            },
            // Styles
            {

                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],

            },
            // Images
            {

                test: /\.(png|svg|jpg|jpeg|gif)$/i,
                type: 'asset/resource',
            },
            // Fonts
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/i,
                type: 'asset/resource',
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            inject: true,
            template: path.resolve(appDirectory, "public/index.html"),
        }),
        new CleanWebpackPlugin(),
    ],
    mode: "development",
};