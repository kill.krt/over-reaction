import "./style.css";

import {
    Scene,
    Engine,
} from "@babylonjs/core";

import FontFaceObserver from "fontfaceobserver";
import { IScene, AppState } from "./scene/iscene"
import { MainMenuScene } from "./scene/mainmenuscene";
import { NewGameScene } from "./scene/newgamescene";

class App {
    private canvas!: HTMLCanvasElement;
    private engine!: Engine;
    private previousAppState: AppState;
    private scenes: IScene[];
    private currentScene?: IScene;

    // Create the Engine and retrieve the canvas
    constructor () {
        // We start from the main menu,
        this.previousAppState = AppState.None;

        // Create list of scenes
        this.scenes = [new MainMenuScene, new NewGameScene];
        this.currentScene = undefined;

        // Initialize all BabylonJS stuff and launch the game interface
        this.initAndLaunch();
    }

    /// Initialize BabylonJS stuff and launch the game interface
    async initAndLaunch() {
        // Load font
        await this.loadFonts();

        this.canvas = this.createCanvas();
        if (this.canvas != null) {
            this.engine = new Engine(this.canvas);
        } else {
            console.error("Canvas object is null");
        }

        if (this.engine == null) {
            console.error("Engine is null");
        }

        this.main();
    }

    private createCanvas(): HTMLCanvasElement {
        // create the canvas html element and attach it to the webpage
        const canvas = document.getElementById(
            "renderCanvas"
        ) as HTMLCanvasElement;

        return canvas;
    }

    private main() {
        this.engine.runRenderLoop(() => {
            // Update the current scene
            var currentState = this.currentScene?.update() ?? AppState.MainMenu;

            if (this.previousAppState != currentState) {
                this.currentScene?.dispose();
                // Find a scene that can manage current state
                var newScene = this.scenes.find((s) => s.isRelatedTo(currentState));
                if (newScene != null) {
                    this.currentScene = newScene;
                    this.currentScene.init(this.engine);
                } else {
                    console.error("No scene for %s", currentState);
                }
            }

            // Render current scene
            this.currentScene?.render();

            // Update the previous state for next cycle
            this.previousAppState = currentState;
        });
    }

    async loadFonts() {
        await Promise.all([
            new FontFaceObserver('Unicode 0024').load(),
            new FontFaceObserver('Domino Farm').load(),
            new FontFaceObserver('Letterblocks').load()
        ]).then(() => {
            console.log('All fonts loaded.');
        });
    }
}

new App();