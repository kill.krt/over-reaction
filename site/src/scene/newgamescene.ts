import { AppState, IScene } from "./iscene";
import {
    Scene,
    Engine,
    ArcRotateCamera,
    Vector3,
    Color4,
    Color3,
    MeshBuilder,
    Camera,
    PointerEventTypes,
    ActionManager,
    ExecuteCodeAction,
    Layer,
    RenderTargetTexture,
    EffectWrapper,
    EffectRenderer,
    SpriteManager,
    Sprite,
} from "@babylonjs/core";

import { GridMaterial } from "@babylonjs/materials";

import { AdvancedDynamicTexture, TextBlock } from "@babylonjs/gui/2D";
import { Broker } from "../api/broker";
import { Surface } from "../geometry/surface";
import { Vector } from "../geometry/vector";

export class NewGameScene implements IScene {
    scene!: Scene;
    currentState!: AppState;
    gui!: AdvancedDynamicTexture;
    canvas!: HTMLCanvasElement;
    divFps!: HTMLElement;
    engine!: Engine;

    constructor () {
    }

    /// Initialize the scene
    init(engine: Engine): void {
        this.divFps = document.getElementById("fps")!;
        this.canvas = engine.getRenderingCanvas()!;
        this.scene = new Scene(engine);
        this.engine = engine;

        // Clean color
        //this.scene.clearColor = new Color4(0.41, 0.4, 0.4, 1);
        this.scene.clearColor = new Color4(0.5, 0.5, 0.75, 1).toLinearSpace();

        // We have just entered the main menu, we need to init
        // main menu stuff
        this.gui = AdvancedDynamicTexture.CreateFullscreenUI("UI");
        this.currentState = AppState.NewGame;

        // Title
        var exit = new TextBlock();
        exit.text = "Exit";
        exit.fontFamily = "Domino Farm";
        exit.fontSize = "8%";
        exit.left = "0%";
        exit.top = "40%";
        exit.color = "black";
        exit.horizontalAlignment = TextBlock.HORIZONTAL_ALIGNMENT_LEFT;
        exit.isPointerBlocker = true;
        exit.resizeToFit = true;

        this.gui.addControl(exit);

        // Highlight in red when mouse hovering
        exit.onPointerEnterObservable.add(
            () => {
                exit.outlineColor = "red";
                exit.outlineWidth = 10;
            }
        );
        // Remove red highlight when mouse move out
        exit.onPointerOutObservable.add(
            () => {
                exit.outlineWidth = 0;
            }
        );
        // On click create switch to new game state
        exit.onPointerClickObservable.add(
            () => {
                this.currentState = AppState.MainMenu;
            }
        );

        // New terrain
        var newTerrain = new TextBlock();
        newTerrain.text = "New terrain";
        newTerrain.fontFamily = "Domino Farm";
        newTerrain.fontSize = "8%";
        newTerrain.left = "0px";
        newTerrain.top = "40%";
        newTerrain.color = "black";
        newTerrain.horizontalAlignment = TextBlock.HORIZONTAL_ALIGNMENT_RIGHT;
        newTerrain.isPointerBlocker = true;
        newTerrain.resizeToFit = true;

        this.gui.addControl(newTerrain);

        // Highlight in red when mouse hovering
        newTerrain.onPointerEnterObservable.add(
            () => {
                newTerrain.outlineColor = "red";
                newTerrain.outlineWidth = 10;
            }
        );
        // Remove red highlight when mouse move out
        newTerrain.onPointerOutObservable.add(
            () => {
                newTerrain.outlineWidth = 0;
            }
        );
        // On click create switch to new game state
        newTerrain.onPointerClickObservable.add(
            () => {
                let mesh;

                do {
                    mesh = this.scene.meshes.pop();
                    mesh?.dispose();
                } while (mesh != null);

                let camera;

                do {
                    camera = this.scene.cameras.pop();
                    camera?.dispose();
                } while (camera != null);

                Broker.FetchData<Surface>("api/data").then((rawSurface) => {
                    let surface = Object.assign(Object.create(Surface.prototype), rawSurface);
                    this.surfaceReady(surface);
                });
            }
        );

        Broker.FetchData<Surface>("api/data").then((rawSurface) => {
            let surface = Object.assign(Object.create(Surface.prototype), rawSurface);
            this.surfaceReady(surface);
        });
    };

    /// Render the scene
    render(): void {
        if (this.scene.cameras.length > 0) {
            this.divFps.innerHTML = this.engine.getFps().toFixed() + "fps";
            this.scene.render();
        }
    }

    /// Dispose the scene
    dispose(): void {
        this.scene.dispose();
        this.gui.dispose();
    }

    /// Returns whether this scene manage a particular app state
    isRelatedTo(appState: AppState): boolean {
        return appState == AppState.NewGame;
    }

    /// Update state
    update(): AppState {
        return this.currentState;
    }

    surfaceReady(surface: Surface) {
        surface.normalize();
        let vol = surface.volume();

        let min = vol.origin;
        let max = new Vector(vol.origin.vector.x + vol.volume.x, vol.origin.vector.y + vol.volume.y, vol.origin.vector.z + vol.volume.z);

        // This creates and positions a free camera (non-mesh)
        let camera = new ArcRotateCamera(
            "camera2",
            Math.PI / 4,
            Math.PI / 4,
            3 * Math.max(max.x, max.y, max.z),
            new Vector3(0, 0, 0),
            this.scene
        );
        camera.mode = Camera.ORTHOGRAPHIC_CAMERA;

        // This attaches the camera to the canvas
        camera.attachControl(this.canvas, true);

        camera.lowerRadiusLimit = camera.radius;
        camera.upperRadiusLimit = camera.radius;
        camera.wheelDeltaPercentage = 0.02;
        //camera.upperRadiusLimit = Math.pow(max.x - min.x, 2);

        let cameraView = Math.max((max.x - min.vector.x) * 1.5, (max.y - min.vector.y) * 1.5);

        camera.orthoLeft = (-1.2 * cameraView) / 2;
        camera.orthoRight = -camera.orthoLeft;
        this.setTopBottomRatio(camera);

        this.scene.onPointerObservable.add(({ event }: any) => {
            const delta = -Math.sign(event.deltaY);
            this.zoom2DView(camera, delta);
        }, PointerEventTypes.POINTERWHEEL);

        let dirtMaterial = new GridMaterial("dirt", this.scene);
        dirtMaterial.lineColor = new Color3(0.2, 0.2, 0.2);
        dirtMaterial.minorUnitVisibility = 0;
        dirtMaterial.gridOffset = new Vector3(0.5, 0.5, 0.5);
        dirtMaterial.majorUnitFrequency = 0.5;
        dirtMaterial.mainColor = new Color3(0.667, 0.4, 0.168);
        dirtMaterial.freeze();

        let grassMaterial = new GridMaterial("grass", this.scene);
        grassMaterial.lineColor = new Color3(0.2, 0.2, 0.2);
        grassMaterial.minorUnitVisibility = 0;
        grassMaterial.gridOffset = new Vector3(0.5, 0.5, 0.5);
        grassMaterial.majorUnitFrequency = 0.5;
        grassMaterial.mainColor = new Color3(0.49, 0.8, 0.05);
        grassMaterial.freeze();

        // Create surface
        surface.blocks.forEach(block => {
            let h = block.block_type == "Flat" ? 0.3 : block.cuboid.volume.z;
            let b = MeshBuilder.CreateBox("box", { width: block.cuboid.volume.x, height: h, depth: block.cuboid.volume.y }, this.scene);

            b.position.x = block.cuboid.origin.vector.x;
            b.position.z = block.cuboid.origin.vector.y;

            if (block.block_type == "Flat") {
                b.position.y = block.cuboid.origin.vector.z + h / 2;
            }
            else {
                b.position.y = block.cuboid.origin.vector.z + h / 2;
            }

            if (block.material == "Grass") {
                b.material = grassMaterial;
            } else {
                b.material = dirtMaterial;
            }
        });

        // Create a render target.
        var rtt = new RenderTargetTexture("", 200, this.scene)

        // Create the background from it
        var background = new Layer("back", null, this.scene);
        background.isBackground = true;
        background.texture = rtt;

        // Create the background effect.
        var renderImage = new EffectWrapper({
            engine: this.scene.getEngine(),
            fragmentShader: `
                varying vec2 vUV;

                void main(void) {
                    float b = 1.0 - abs(0.5-vUV.y);
                    gl_FragColor = vec4(0.3 * b, 0.5 * b, b, 1.0);

                }
            `
        });

        // When the effect has been ready,
        // Create the effect render and change which effects will be renderered

        renderImage.effect.executeWhenCompiled(() => {
            // Render the effect in the RTT.
            var renderer = new EffectRenderer(this.scene.getEngine());
            renderer.render(renderImage, rtt);
        });
    }

    setTopBottomRatio(camera: ArcRotateCamera) {
        const ratio = this.canvas.height / this.canvas.width;
        if (camera.orthoLeft && camera.orthoRight) {
            camera.orthoTop = camera.orthoRight * ratio;
            camera.orthoBottom = camera.orthoLeft * ratio;
        }
    }

    zoom2DView(camera: ArcRotateCamera, delta: number) {
        const zoomingOut = delta < 0;

        if (camera.orthoLeft && camera.orthoRight) {
            // limit zooming in to no less than 3 units.
            if (!zoomingOut && Math.abs(camera.orthoLeft) <= 3) {
                return;
            }

            camera.orthoLeft += delta / 4;
            camera.orthoRight -= delta / 4;

            this.setTopBottomRatio(camera);
        }
    }
}