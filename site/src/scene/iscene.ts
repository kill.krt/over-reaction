import {
    Engine,
} from "@babylonjs/core";

export enum AppState {
    None,
    Error,
    MainMenu,
    NewGame
}

export interface IScene {
    /// Initialize the scene
    init(engine: Engine): void;

    /// Render the scene
    render(): void;

    /// Dispose the scene
    dispose(): void;

    /// Update state
    update(): AppState;

    /// Returns whether this scene manage a particular app state
    isRelatedTo(appState: AppState): boolean;
}