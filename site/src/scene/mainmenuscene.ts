import { AppState, IScene } from "./iscene";
import {
    Scene,
    Engine,
    ArcRotateCamera,
    Vector3,
    Color4,
    Color3,
} from "@babylonjs/core";

import { AdvancedDynamicTexture } from "@babylonjs/gui/2D";
import { TextBlock } from "@babylonjs/gui/2D/controls/textBlock";

export class MainMenuScene implements IScene {
    scene!: Scene;
    title!: TextBlock;
    i: number;
    currentState!: AppState;
    gui!: AdvancedDynamicTexture;

    constructor () {
        this.i = 0;
    }

    /// Initialize the scene
    init(engine: Engine): void {
        // We have just entered the main menu, we need to init
        // main menu stuff
        this.currentState = AppState.MainMenu;
        this.scene = new Scene(engine);
        this.gui = AdvancedDynamicTexture.CreateFullscreenUI("UI");

        // Clean color
        this.scene.clearColor = new Color4(0.81, 0.8, 0.8, 1);

        // Title
        this.title = new TextBlock();
        this.title.fontFamily = "Letterblocks";
        this.title.text = "OverReaction";
        this.title.fontSize = "15%";
        this.title.top = "40%";
        this.title.color = "black";
        this.gui.addControl(this.title);

        // New game
        var newGame = new TextBlock();
        newGame.fontFamily = "Domino Farm";
        newGame.text = "New game";
        newGame.fontSize = "8%";
        newGame.top = "0%";
        newGame.color = "black";
        newGame.isPointerBlocker = true;
        newGame.resizeToFit = true;

        // Highlight in red when mouse hovering
        newGame.onPointerEnterObservable.add(
            () => {
                newGame.outlineColor = "red";
                newGame.outlineWidth = 10;
            }
        );
        // Remove red highlight when mouse move out
        newGame.onPointerOutObservable.add(
            () => {
                newGame.outlineWidth = 0;
            }
        );
        // On click create switch to new game state
        newGame.onPointerClickObservable.add(
            () => {
                this.currentState = AppState.NewGame;
            }
        );

        this.gui.addControl(newGame);

        // Camera
        var canvas = engine.getRenderingCanvas();
        var camera = new ArcRotateCamera(
            "Camera",
            -Math.PI / 2,
            Math.PI / 3,
            8,
            Vector3.Zero(),
            this.scene
        );
        camera.attachControl(canvas, true);
        camera.wheelDeltaPercentage = 0.01;
        camera.lowerRadiusLimit = 3.5;
    }

    /// Render the scene
    render(): void {
        var newText = "ReactionOver";

        if (Math.floor((this.i % 50) / 25) == 0) {
            newText = "OverReaction";
        }

        this.title.text = newText;
        this.scene.render();
        this.i++;
    }

    /// Dispose the scene
    dispose(): void {
        this.scene.dispose();
        this.gui.dispose();

    }

    /// Returns whether this scene manage a particular app state
    isRelatedTo(appState: AppState): boolean {
        return appState == AppState.MainMenu;
    }

    /// Update state
    update(): AppState {
        return this.currentState;
    }
}