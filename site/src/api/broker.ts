export class Broker {
    /// Fetch a HTTP GET that return an object serialized in JSON and convert to object
    static async FetchData<T>(url: string): Promise<T> {
        return fetch(url)
            .then(response => {
                if (!response.ok) {
                    // Something went wrong
                    console.error(response.statusText);
                    throw new Error(response.statusText)
                }
                let promiseData: Promise<T> = response.json();
                return promiseData;
            })
    }
}