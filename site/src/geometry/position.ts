import { Vector } from "./vector";

/// Represent a position in 3D space
export class Position {
    /// Position coordinates
    public vector: Vector;

    constructor (vector: Vector) {
        this.vector = vector;
    }
}