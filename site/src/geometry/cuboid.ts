import { Position } from "./position";
import { Vector } from "./vector";

/// Represent a cuboid
///
/// Origin points to the bottom left point (with min y) of the volume
/// Top point at opposite side of the origin is considered not
/// included in the volume
export class Cuboid {
    /// Origin points to the bottom left point (with min y) of the volume
    public origin: Position;
    /// Volume of the volume along each axis
    public volume: Vector;

    constructor (origin: Position, volume: Vector) {
        this.origin = origin;
        this.volume = volume;
    }
}