/// Represent a generic vector of integers
export class Vector {
    public x: number;
    public y: number;
    public z: number;

    constructor (x: number, y: number, z: number) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    module(): number {
        return this.x * this.y * this.z;
    }
}