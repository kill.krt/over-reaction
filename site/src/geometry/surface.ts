/// Represents a collection of non-overlapping blocks forming a surface/terrain where

import { Block } from "./block";
import { Cuboid } from "./cuboid";
import { Position } from "./position";
import { Vector } from "./vector";

/// players can place their pieces.
export class Surface {
    /// List of all not overlapping blocks
    blocks!: Block[];

    volume(): Cuboid {
        let maxX = Math.max(... this.blocks.map(block => block.cuboid.origin.vector.x));
        let maxY = Math.max(... this.blocks.map(block => block.cuboid.origin.vector.y));
        let maxZ = Math.max(... this.blocks.map(block => block.cuboid.origin.vector.z));

        let minX = Math.min(... this.blocks.map(block => block.cuboid.origin.vector.x));
        let minY = Math.min(... this.blocks.map(block => block.cuboid.origin.vector.y));
        let minZ = Math.min(... this.blocks.map(block => block.cuboid.origin.vector.z));

        return new Cuboid(new Position(new Vector(minX, minY, minZ)),
            new Vector(maxX - minX + 1, maxY - minY + 1, maxZ - minZ + 1)
        );
    }

    normalize() {
        let volume = this.volume();
        let deltax = volume.volume.x / 2 - 0.5;
        let deltay = volume.volume.y / 2 - 0.5;
        let deltaz = volume.volume.z / 2 - 0.5;

        this.blocks.forEach(block => {
            block.cuboid.origin.vector.x -= deltax + volume.origin.vector.x;
            block.cuboid.origin.vector.y -= deltay + volume.origin.vector.y;
            block.cuboid.origin.vector.z -= deltaz + volume.origin.vector.z;
        });
    }
}