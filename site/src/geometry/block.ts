import { Cuboid } from "./cuboid";

export enum BlockType {
    /// Block with a flat surface where a piece can be placed
    Flat,
    /// Filled block where nothing can be placed
    Fill,
}

/// All materials that a block can be built with
export enum BlockMaterial {
    Sand,
    Rock,
    Water,
    Grass,
    Ice,
    Dirt
}

/// Represents a block of a playing surface
export class Block {
    /// Block material
    material: string;
    /// Type of block
    block_type: string;
    /// Block geometry
    cuboid: Cuboid;

    constructor (material: string, block_type: string, cuboid: Cuboid) {
        this.material = material;
        this.block_type = block_type;
        this.cuboid = cuboid;
    }
}