use serde::Serialize;
use std::ops::{Add, Deref, Sub};

use super::{
    distance::Distance,
    vector::{Scalar, Vector},
};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize)]
/// Represent a position in 3D space
pub struct Position {
    /// Position coordinates
    vector: Vector,
}

impl Position {
    /// Create a new position with provided coordinates
    pub fn new(x: Scalar, y: Scalar, z: Scalar) -> Self {
        Self {
            vector: Vector { x, y, z },
        }
    }

    /// Returns coordinate of the position
    pub fn coordinates(&self) -> Vector {
        self.vector
    }
}

impl From<Vector> for Position {
    /// Convert a Vector to a Position
    fn from(v: Vector) -> Self {
        Self { vector: v }
    }
}

impl Add<Distance> for Position {
    type Output = Self;

    /// Add a distance to a position. The sum will be the vector sum between the two
    fn add(self, other: Distance) -> Self::Output {
        Self::from(self.vector + other.coordinates())
    }
}

impl Sub<Position> for Position {
    type Output = Distance;

    /// Subtract two position to get their distance
    fn sub(self, other: Position) -> Self::Output {
        Distance::from(self.vector - other.vector)
    }
}

impl Deref for Position {
    type Target = Vector;

    fn deref(&self) -> &Self::Target {
        &self.vector
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        common::random_generators::random_vector,
        geometry::{distance::Distance, position::Position, vector::Scalar},
        test_utilities::constants::NUMBER_OF_LOOPS_FOR_NORMAL_TEST,
    };

    #[test]
    fn from_test() {
        for _ in 0..NUMBER_OF_LOOPS_FOR_NORMAL_TEST {
            let v = random_vector(Scalar::MIN, Scalar::MAX);
            let p = Position::from(v);

            assert_eq!(v, p.coordinates());
        }
    }

    #[test]
    fn add_test() {
        for _ in 0..NUMBER_OF_LOOPS_FOR_NORMAL_TEST {
            let v0 = random_vector(-100, 100);
            let v1 = random_vector(-100, 100);
            let p = Position::from(v0);
            let d = Distance::from(v1);

            let p1 = p + d;

            assert_eq!(p1.coordinates(), v0 + v1);
        }
    }

    #[test]
    fn sub_test() {
        for _ in 0..NUMBER_OF_LOOPS_FOR_NORMAL_TEST {
            let v0 = random_vector(-100, 100);
            let v1 = random_vector(-100, 100);
            let p0 = Position::from(v0);
            let p1 = Position::from(v1);

            let d = p1 - p0;

            assert_eq!(d.coordinates(), v1 - v0);
        }
    }

    #[test]
    fn new_test() {
        for _ in 0..NUMBER_OF_LOOPS_FOR_NORMAL_TEST {
            let v = random_vector(Scalar::MIN, Scalar::MAX);
            let p = Position::new(v.x, v.y, v.z);

            assert_eq!(v, p.coordinates());
        }
    }
}
