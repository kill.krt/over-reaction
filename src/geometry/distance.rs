use std::ops::Deref;

use super::vector::{Scalar, Vector};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
/// Represent a distance in 3D space
pub struct Distance {
    vector: Vector,
}

impl Distance {
    /// Create a new distance with provided coordinates
    pub fn new(x: Scalar, y: Scalar, z: Scalar) -> Self {
        Self {
            vector: Vector { x, y, z },
        }
    }

    /// Returns coordinate of the distance
    pub fn coordinates(&self) -> Vector {
        self.vector
    }
}

impl From<Vector> for Distance {
    /// Convert a Vector to a Distance
    fn from(v: Vector) -> Self {
        Self { vector: v }
    }
}

impl Deref for Distance {
    type Target = Vector;

    fn deref(&self) -> &Vector {
        &self.vector
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        common::random_generators::random_vector,
        geometry::{distance::Distance, vector::Scalar},
        test_utilities::constants::NUMBER_OF_LOOPS_FOR_NORMAL_TEST,
    };

    #[test]
    fn new_test() {
        for _ in 0..NUMBER_OF_LOOPS_FOR_NORMAL_TEST {
            let v = random_vector(Scalar::MIN, Scalar::MAX);
            let d = Distance::new(v.x, v.y, v.z);

            assert_eq!(v, d.coordinates());
        }
    }

    #[test]
    fn from_test() {
        for _ in 0..NUMBER_OF_LOOPS_FOR_NORMAL_TEST {
            let v = random_vector(Scalar::MIN, Scalar::MAX);
            let d = Distance::from(v);

            assert_eq!(v, d.coordinates());
        }
    }
}
