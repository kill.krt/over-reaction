use std::ops::{Add, Neg, Sub};

use serde::Serialize;

/// Type used to store Vector components
pub type Scalar = i32;

/// Represent a generic vector of integers
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, Serialize)]
pub struct Vector {
    pub x: Scalar,
    pub y: Scalar,
    pub z: Scalar,
}

impl Vector {
    /// Create a new vector with given components
    pub fn new(x: Scalar, y: Scalar, z: Scalar) -> Self {
        Self { x, y, z }
    }

    /// Returns true if all components are equal or greater than 0
    pub fn is_positive(&self) -> bool {
        self.x >= 0 && self.y >= 0 && self.z >= 0
    }

    /// Returns true if all components are equal to 0
    pub fn is_zero(&self) -> bool {
        self.x == 0 && self.y == 0 && self.z == 0
    }

    /// Returns a vector with components equal to provided one, but with absolute values
    pub fn abs(&self) -> Self {
        Self::new(self.x.abs(), self.y.abs(), self.z.abs())
    }

    /// Returns a vector with all components set to zero.
    pub fn zero() -> Self {
        Self::new(0, 0, 0)
    }

    /// Returns true if all components are greater than 0
    pub fn is_greater_than_zero(&self) -> bool {
        self.x > 0 && self.y > 0 && self.z > 0
    }
}

impl Add<Vector> for Vector {
    type Output = Vector;

    /// Add vector components each other
    fn add(self, other: Vector) -> Self::Output {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl Sub<Vector> for Vector {
    type Output = Vector;

    /// Subtract vector components each other
    fn sub(self, other: Vector) -> Self::Output {
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

impl Neg for Vector {
    type Output = Self;

    /// Return a vector with all components negated
    fn neg(self) -> Self::Output {
        Self {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        common::random_generators::{random_number, random_vector},
        test_utilities::constants::*,
    };

    #[test]
    /// Check if is_positive returns only for vector with only
    /// positive components
    fn is_positive_test() {
        for _ in 0..NUMBER_OF_LOOPS_FOR_BIG_TEST {
            // Random number
            let x0 = random_number(-100, 100);
            let y0 = random_number(-100, 100);
            let z0 = random_number(-100, 100);
            // Vector
            let v = Vector::new(x0, y0, z0);

            assert_eq!(v.is_positive(), x0 >= 0 && y0 >= 0 && z0 >= 0);
            assert_eq!(!v.is_positive(), x0 < 0 || y0 < 0 || z0 < 0);
        }
    }

    #[test]
    /// Check if is_zero returns only for vector with only
    /// 0 components
    fn is_zero_test() {
        assert!(Vector::zero().is_zero());

        for _ in 0..NUMBER_OF_LOOPS_FOR_BIG_TEST {
            // Random number
            let x0 = random_number(-100, 100);
            let y0 = random_number(-100, 100);
            let z0 = random_number(-100, 100);
            // Vector
            let v = Vector::new(x0, y0, z0);

            assert_eq!(v.is_zero(), x0 == 0 && y0 == 0 && z0 == 0);
            assert_eq!(!v.is_zero(), x0 != 0 || y0 != 0 || z0 != 0);
        }
    }

    #[test]
    /// Check if add compute right values
    fn add_test() {
        for _ in 0..NUMBER_OF_LOOPS_FOR_NORMAL_TEST {
            // Vector 0 components
            let x0 = random_number(-100, 100);
            let y0 = random_number(-100, 100);
            let z0 = random_number(-100, 100);
            // Vector 1 components
            let x1 = random_number(-100, 100);
            let y1 = random_number(-100, 100);
            let z1 = random_number(-100, 100);

            let v0 = Vector::new(x0, y0, z0);
            let v1 = Vector::new(x1, y1, z1);

            let v_sum = v0 + v1;

            assert_eq!(v_sum.x, x0 + x1);
            assert_eq!(v_sum.y, y0 + y1);
            assert_eq!(v_sum.z, z0 + z1);
        }
    }

    #[test]
    /// Check if sub compute right values
    fn sub_test() {
        for _ in 0..NUMBER_OF_LOOPS_FOR_NORMAL_TEST {
            // Vector 0 components
            let x0 = random_number(-100, 100);
            let y0 = random_number(-100, 100);
            let z0 = random_number(-100, 100);
            // Vector 1 components
            let x1 = random_number(-100, 100);
            let y1 = random_number(-100, 100);
            let z1 = random_number(-100, 100);

            let v0 = Vector::new(x0, y0, z0);
            let v1 = Vector::new(x1, y1, z1);

            let v_sum = v0 - v1;

            assert_eq!(v_sum.x, x0 - x1);
            assert_eq!(v_sum.y, y0 - y1);
            assert_eq!(v_sum.z, z0 - z1);
        }
    }

    #[test]
    /// Check if constructor does save provided parameters in right way
    fn constructor_test() {
        for _ in 0..NUMBER_OF_LOOPS_FOR_NORMAL_TEST {
            // Vector 0 components
            let x0 = random_number(-100, 100);
            let y0 = random_number(-100, 100);
            let z0 = random_number(-100, 100);

            let v0 = Vector::new(x0, y0, z0);

            assert_eq!(v0.x, x0);
            assert_eq!(v0.y, y0);
            assert_eq!(v0.z, z0);
        }
    }

    #[test]
    /// Check if abs is computing a vector with all components with absolute value
    fn abs_test() {
        for _ in 0..NUMBER_OF_LOOPS_FOR_NORMAL_TEST {
            let v = random_vector(-100, 100);
            let av = v.abs();

            assert_eq!(v.x.abs(), av.x);
            assert_eq!(v.y.abs(), av.y);
            assert_eq!(v.z.abs(), av.z);
        }
    }

    #[test]
    /// Check if v + 0 = v
    fn add_zero() {
        for _ in 0..NUMBER_OF_LOOPS_FOR_NORMAL_TEST {
            // Vector 0 components
            let x0 = random_number(-100, 100);
            let y0 = random_number(-100, 100);
            let z0 = random_number(-100, 100);

            let v0 = Vector::new(x0, y0, z0);

            let v_sum = v0 + Vector::zero();

            assert_eq!(v_sum.x, x0);
            assert_eq!(v_sum.y, y0);
            assert_eq!(v_sum.z, z0);
        }
    }

    #[test]
    /// Check if v - 0 = v
    fn sub_zero() {
        for _ in 0..NUMBER_OF_LOOPS_FOR_NORMAL_TEST {
            // Vector 0 components
            let x0 = random_number(-100, 100);
            let y0 = random_number(-100, 100);
            let z0 = random_number(-100, 100);

            let v0 = Vector::new(x0, y0, z0);

            let v_sum = v0 - Vector::zero();

            assert_eq!(v_sum.x, x0);
            assert_eq!(v_sum.y, y0);
            assert_eq!(v_sum.z, z0);
        }
    }

    #[test]
    /// Check if v - v = 0
    fn sum_to_zero() {
        for _ in 0..NUMBER_OF_LOOPS_FOR_NORMAL_TEST {
            // Vector 0 components
            let x0 = random_number(-100, 100);
            let y0 = random_number(-100, 100);
            let z0 = random_number(-100, 100);
            let v0 = Vector::new(x0, y0, z0);

            // Vector 1 as -v0
            let v1 = Vector::new(-x0, -y0, -z0);
            let v2 = v0;
            let v0_sum = v0 - v2;
            let v1_sum = v0 + v1;

            assert_eq!(v0_sum.x, 0);
            assert_eq!(v0_sum.y, 0);
            assert_eq!(v0_sum.z, 0);

            assert_eq!(v1_sum.x, 0);
            assert_eq!(v1_sum.y, 0);
            assert_eq!(v1_sum.z, 0);

            assert_eq!(v0_sum, Vector::zero());
            assert_eq!(v1_sum, Vector::zero());
        }
    }

    #[test]
    /// Subtract a random vector to vector 0
    fn subtract_to_zero() {
        for _ in 0..NUMBER_OF_LOOPS_FOR_NORMAL_TEST {
            // Vector 0 components
            let x0 = random_number(-100, 100);
            let y0 = random_number(-100, 100);
            let z0 = random_number(-100, 100);
            let v0 = Vector::new(x0, y0, z0);

            // Vector 1 as -v0
            let sub = Vector::zero() - v0;

            assert_eq!(sub.x, -x0);
            assert_eq!(sub.y, -y0);
            assert_eq!(sub.z, -z0);
        }
    }

    #[test]
    fn neg_test() {
        for _ in 0..NUMBER_OF_LOOPS_FOR_NORMAL_TEST {
            // Vector 0 components
            let x0 = random_number(-100, 100);
            let y0 = random_number(-100, 100);
            let z0 = random_number(-100, 100);
            let v0 = Vector::new(x0, y0, z0);

            // neg as -v0
            let neg = -v0;

            assert_eq!(neg.x, -x0);
            assert_eq!(neg.y, -y0);
            assert_eq!(neg.z, -z0);

            // Twice negation means identity
            let identity = -neg;
            assert_eq!(identity, v0);
            assert_eq!(-(-v0), v0);
        }
    }

    #[test]
    fn is_greater_than_zero_test() {
        assert!(!Vector::zero().is_greater_than_zero());

        for _ in 0..NUMBER_OF_LOOPS_FOR_BIG_TEST {
            // Random number
            let x0 = random_number(-100, 100);
            let y0 = random_number(-100, 100);
            let z0 = random_number(-100, 100);
            // Vector
            let v = Vector::new(x0, y0, z0);

            assert_eq!(v.is_greater_than_zero(), x0 > 0 && y0 > 0 && z0 > 0);
        }
    }
}
