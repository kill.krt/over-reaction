use serde::Serialize;
use std::ops::Deref;

use super::cuboid::Cuboid;

/// All types of block
#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize)]
pub enum BlockType {
    /// Block with a flat surface where a piece can be placed
    Flat,
    /// Filled block where nothing can be placed
    Fill,
}

/// All materials that a block can be built with
#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize)]
pub enum BlockMaterial {
    Sand,
    Rock,
    Water,
    Grass,
    Ice,
    Dirt,
}

/// Represents a block of a playing surface
#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize)]
pub struct Block {
    /// Block material
    material: BlockMaterial,
    /// Type of block
    block_type: BlockType,
    /// Block geometry
    cuboid: Cuboid,
}

impl Block {
    /// Create a new block with provided data
    pub fn new(cuboid: Cuboid, material: BlockMaterial, block_type: BlockType) -> Self {
        Self {
            cuboid,
            material,
            block_type,
        }
    }

    /// Gets the geometry information of this block
    pub fn cuboid(&self) -> Cuboid {
        self.cuboid
    }

    /// Gets the material of this block
    pub fn material(&self) -> BlockMaterial {
        self.material
    }

    /// Gets the type of this block
    pub fn block_type(&self) -> BlockType {
        self.block_type
    }
}

/// A block can be coerced to a cuboid
impl Deref for Block {
    type Target = Cuboid;

    fn deref(&self) -> &Self::Target {
        &self.cuboid
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        common::random_generators::{
            random_block, random_block_material, random_block_type, random_cuboid,
        },
        geometry::cuboid::Cuboid,
        test_utilities::constants::NUMBER_OF_LOOPS_FOR_NORMAL_TEST,
    };

    use super::Block;

    #[test]
    fn new_test() {
        for _ in 0..NUMBER_OF_LOOPS_FOR_NORMAL_TEST {
            let material = random_block_material();
            let block_type = random_block_type();
            let cuboid = random_cuboid();

            let block = Block::new(cuboid, material, block_type);

            // Check if getters methods work
            assert_eq!(material, block.material());
            assert_eq!(block_type, block.block_type());
            assert_eq!(cuboid, block.cuboid());
        }
    }

    fn foo(cuboid: &Cuboid) -> Cuboid {
        *cuboid
    }

    #[test]
    fn coercion_test() {
        for _ in 0..NUMBER_OF_LOOPS_FOR_NORMAL_TEST {
            let block = random_block();
            let cuboid = foo(&block);

            assert_eq!(block.cuboid(), cuboid);
        }
    }
}
