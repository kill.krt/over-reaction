use log::warn;
use serde::Serialize;

use super::{
    position::Position,
    vector::{Scalar, Vector},
};

/// Represent a cuboid
///
/// Origin points to the bottom left point (with min y) of the volume
/// Top point at opposite side of the origin is considered not
/// included in the volume
#[derive(Debug, Copy, Clone, PartialEq, Eq, Serialize)]
pub struct Cuboid {
    /// Origin points to the bottom left point (with min y) of the volume
    origin: Position,
    /// Volume of the volume along each axis
    volume: Vector,
}

impl Cuboid {
    /// Creates a new cuboid with provided data
    pub fn new(origin: Position, volume: Vector) -> Option<Cuboid> {
        // Size must be greater than 0 along all axis
        if !volume.is_greater_than_zero() {
            warn!("Provided volume is not valid: {:?}", volume);
            None
        } else {
            Some(Self { origin, volume })
        }
    }

    /// Gets the origin of this cuboid
    pub fn origin(&self) -> Position {
        self.origin
    }

    /// Gets the size of this cuboid
    pub fn volume(&self) -> Vector {
        self.volume
    }

    fn is_overlapping_with(&self, other: &Cuboid) -> bool {
        let x_a_min = self.origin.x;
        let y_a_min = self.origin.y;
        let z_a_min = self.origin.z;
        let x_a_max = self.origin.x + self.volume.x;
        let y_a_max = self.origin.y + self.volume.y;
        let z_a_max = self.origin.z + self.volume.z;

        let x_b_min = other.origin.x;
        let y_b_min = other.origin.y;
        let z_b_min = other.origin.z;
        let x_b_max = other.origin.x + other.volume.x;
        let y_b_max = other.origin.y + other.volume.y;
        let z_b_max = other.origin.z + other.volume.z;

        ((x_a_min >= x_b_min && x_a_min < x_b_max)
            || (x_a_max > x_b_min && x_a_max <= x_b_max)
            || (x_a_min <= x_b_min && x_a_max >= x_b_max))
            && ((y_a_min >= y_b_min && y_a_min < y_b_max)
                || (y_a_max > y_b_min && y_a_max <= y_b_max)
                || (y_a_min <= y_b_min && y_a_max >= y_b_max))
            && ((z_a_min >= z_b_min && z_a_min < z_b_max)
                || (z_a_max > z_b_min && z_a_max <= z_b_max)
                || (z_a_min <= z_b_min && z_a_max >= z_b_max))
    }

    /// Check if this cuboid and passed one share at least one point
    pub fn are_overlapping(&self, other: &Cuboid) -> bool {
        self.is_overlapping_with(other) || other.is_overlapping_with(self)
    }

    /// Check if a point is inside the cuboid volume
    pub fn contains(&self, position: Position) -> bool {
        let x_a_min = self.origin.x;
        let y_a_min = self.origin.y;
        let z_a_min = self.origin.z;
        let x_a_max = self.origin.x + self.volume.x;
        let y_a_max = self.origin.y + self.volume.y;
        let z_a_max = self.origin.z + self.volume.z;

        let x = position.x;
        let y = position.y;
        let z = position.z;

        ((x >= x_a_min && x < x_a_max)
            || (x > x_a_min && x < x_a_max)
            || (x <= x_a_min && x >= x_a_max))
            && ((y >= y_a_min && y < y_a_max)
                || (y > y_a_min && y < y_a_max)
                || (y <= y_a_min && y >= y_a_max))
            && ((z >= z_a_min && z < z_a_max)
                || (z > z_a_min && z < z_a_max)
                || (z <= z_a_min && z >= z_a_max))
    }
}

/// Iterator for returning all position inside a cuboid
pub struct CuboidIterator {
    index: Scalar,
    origin: Position,
    volume: Vector,
}

impl Iterator for CuboidIterator {
    type Item = Position;

    /// Provides all point starting from origin and moving
    /// from left to right, close to far, bottom to up
    fn next(&mut self) -> std::option::Option<Self::Item> {
        if self.index >= self.volume.x * self.volume.y * self.volume.z {
            // We reach the end of the volume
            None
        } else {
            let x = self.origin.x + self.index % self.volume.x;
            let y =
                self.origin.y + ((self.index % (self.volume.x * self.volume.y)) / self.volume.x);
            let z = self.origin.z + (self.index / (self.volume.x * self.volume.y));

            self.index += 1;
            Some(Position::new(x, y, z))
        }
    }
}

impl<'a> IntoIterator for &'a Cuboid {
    type Item = Position;
    type IntoIter = CuboidIterator;

    /// Return an iterator that iterate on all points inside the cuboid
    fn into_iter(self) -> Self::IntoIter {
        CuboidIterator {
            index: 0,
            origin: self.origin,
            volume: self.volume,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        common::random_generators::{random_number, random_position, random_vector},
        geometry::{
            cuboid::Cuboid,
            position::Position,
            vector::{Scalar, Vector},
        },
        test_utilities::constants::{
            NUMBER_OF_LOOPS_FOR_NORMAL_TEST, NUMBER_OF_LOOPS_FOR_SMALL_TEST,
            NUMBER_OF_LOOPS_FOR_TINY_TEST,
        },
    };

    #[test]
    fn new_test() {
        for _ in 0..NUMBER_OF_LOOPS_FOR_NORMAL_TEST {
            let position = random_position();
            let size = random_vector(-100, 100);

            let volume = Cuboid::new(position, size);

            // Cuboid is valid only if size is greater than zero
            assert_eq!(size.is_greater_than_zero(), volume.is_some());
        }
    }

    #[test]
    fn getters_test() {
        for _ in 0..NUMBER_OF_LOOPS_FOR_NORMAL_TEST {
            let position = random_position();
            let size = random_vector(-100, 100);

            let volume = Cuboid::new(position, size);

            // Check if the getter methods return correct stuff
            if let Some(actual_volume) = volume {
                assert_eq!(position, actual_volume.origin());
                assert_eq!(size, actual_volume.volume());
            }
        }
    }

    #[test]
    fn into_iter_test() {
        {
            // Unit volume
            let origin = random_position();
            let volume = Cuboid::new(origin, Vector::new(1, 1, 1)).unwrap();

            assert_eq!(1, volume.into_iter().count());

            // Should contain only one point
            assert_eq!(Some(origin), volume.into_iter().next());
        }
        {
            let origin = random_position();
            for l in 1..NUMBER_OF_LOOPS_FOR_TINY_TEST as Scalar {
                // Create a volume lxlxl
                let volume = Cuboid::new(origin, Vector::new(l, l, l)).unwrap();

                // It should contain l*l*l blocks
                assert_eq!(l * l * l, volume.into_iter().count() as Scalar);

                // It should contain all points between [x, x+l)[y, y+l)[z, z+l)]
                for x in origin.x..origin.x + l {
                    for y in origin.y..origin.y + l {
                        for z in origin.z..origin.z + l {
                            assert!(volume.into_iter().any(|p| p == Position::new(x, y, z)));
                        }
                    }
                }
            }
        }
    }

    fn random_little_cuboid() -> Cuboid {
        let size = random_vector(1, 6);
        let origin = Position::from(random_vector(-5, 5));

        Cuboid::new(origin, size).unwrap()
    }

    #[test]
    fn are_overlapping_test() {
        for _ in 0..NUMBER_OF_LOOPS_FOR_NORMAL_TEST {
            let c0 = random_little_cuboid();
            let c1 = random_little_cuboid();

            let are_overlapping = c0.into_iter().any(|p| c1.into_iter().any(|q| p == q));

            assert_eq!(
                are_overlapping,
                c0.are_overlapping(&c1),
                "{:?} - {:?}",
                c0,
                c1
            );

            assert_eq!(
                are_overlapping,
                c1.are_overlapping(&c0),
                "{:?} - {:?}",
                c0,
                c1
            );
        }
    }

    #[test]
    fn contains_test() {
        for l in 1..NUMBER_OF_LOOPS_FOR_TINY_TEST as Scalar {
            // Create a volume lxlxl
            let volume = Cuboid::new(Position::new(0, 0, 0), Vector::new(l, l, l)).unwrap();

            // Check for inside points
            for p in &volume {
                assert!(volume.contains(p));
            }

            // Check for outside points
            const MIN: i32 = -100;
            const MAX: i32 = 100;
            for _ in 0..NUMBER_OF_LOOPS_FOR_SMALL_TEST {
                // Check for points on the left
                let neg_x_p = Position::new(
                    random_number(MIN, -1),
                    random_number(MIN, MAX),
                    random_number(MIN, MAX),
                );
                assert!(!volume.contains(neg_x_p));

                // Check for points on the front
                let neg_y_p = Position::new(
                    random_number(MIN, MAX),
                    random_number(MIN, -1),
                    random_number(MIN, MAX),
                );
                assert!(!volume.contains(neg_y_p));

                // Check for points on the bottom
                let neg_z_p = Position::new(
                    random_number(MIN, MAX),
                    random_number(MIN, MAX),
                    random_number(MIN, -1),
                );
                assert!(!volume.contains(neg_z_p));
                // Check for all points on the right
                let pos_x_p = Position::new(
                    random_number(l, MAX),
                    random_number(MIN, MAX),
                    random_number(MIN, MAX),
                );
                assert!(!volume.contains(pos_x_p), "{} - {:?}", l, pos_x_p);
                // Check for all points on the rear
                let pos_y_p = Position::new(
                    random_number(MIN, MAX),
                    random_number(l, MAX),
                    random_number(MIN, MAX),
                );
                assert!(!volume.contains(pos_y_p), "{} - {:?}", l, pos_y_p);

                // Check for all points on the top
                let pos_z_p = Position::new(
                    random_number(MIN, MAX),
                    random_number(MIN, MAX),
                    random_number(l, MAX),
                );
                assert!(!volume.contains(pos_z_p), "{} - {:?}", l, pos_z_p);
            }
        }
    }
}
