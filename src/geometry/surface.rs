use serde::Serialize;

use crate::common::random_generators::{random_bool, random_number};

use super::{
    block::{Block, BlockMaterial, BlockType},
    cuboid::Cuboid,
    position::Position,
    vector::{Scalar, Vector},
};

/// Represents a collection of non-overlapping blocks forming a surface/terrain where
/// players can place their pieces.
#[derive(Debug, Serialize)]
pub struct Surface {
    /// List of all not overlapping blocks
    blocks: Vec<Block>,
}

impl Surface {
    /// Create an empty surface with no blocks
    pub fn new() -> Self {
        Self { blocks: vec![] }
    }

    /// Add a new block to this surface if it is not overlapping an existing one
    /// Returns `true` only if the block was not overlapping another one
    pub fn add_block(&mut self, block: Block) -> bool {
        //Check if the block is not overlapping with other ones
        if self
            .blocks
            .iter()
            .any(|other| other.are_overlapping(&block))
        {
            false
        } else {
            self.blocks.push(block);
            true
        }
    }

    /// Returns the block that contains the specified position
    pub fn block_at(&self, position: Position) -> Option<Block> {
        self.blocks
            .iter()
            .find(|block| block.contains(position))
            .copied()
    }

    /// Returns the volume that contains all block inside the surface
    pub fn volume(&self) -> Option<Cuboid> {
        if self.blocks.is_empty() {
            // No block, it useless to continue
            return None;
        }

        let min_pos = self
            .blocks
            .iter()
            .fold((i32::MAX, i32::MAX, i32::MAX), |min, block| {
                (
                    i32::min(min.0, block.origin().x),
                    i32::min(min.1, block.origin().y),
                    i32::min(min.2, block.origin().z),
                )
            });

        let max_pos = self
            .blocks
            .iter()
            .fold((i32::MIN, i32::MIN, i32::MIN), |max, block| {
                (
                    i32::max(max.0, block.origin().x + block.volume().x),
                    i32::max(max.1, block.origin().y + block.volume().y),
                    i32::max(max.2, block.origin().z + block.volume().z),
                )
            });

        let volume = Vector::new(
            max_pos.0 - min_pos.0,
            max_pos.1 - min_pos.1,
            max_pos.2 - min_pos.2,
        );

        Cuboid::new(Position::new(min_pos.0, min_pos.1, min_pos.2), volume)
    }

    /// Return an iterator that iterate on all blocks piled at selected coordinate (starting from the one with lowest Z)
    fn get_blocks_pile(&self, x: Scalar, y: Scalar) -> impl Iterator<Item = &Block> {
        // Get all block at position x,y
        let mut blocks: Vec<&Block> = self
            .blocks
            .iter()
            .filter(move |&b| b.origin().x == x && b.origin().y == y)
            .collect();

        // Sort by Z coordinate
        blocks.sort_by(|&a, &b| a.origin().z.cmp(&b.origin().z));

        blocks.into_iter()
    }

    /// Generate a random terrain providing the width and height
    ///
    /// The generated surface will contain a terrain with mountains and valleys
    pub fn generate_random_terrain(
        width: Scalar,
        height: Scalar,
        material: BlockMaterial,
    ) -> Surface {
        let mut surface = Surface::new();

        Self::add_terrain_layer(&mut surface, 0, 0, width, height, 0);

        // Check if terrain is completely empty, in case a basic square terrain
        if surface.blocks.is_empty() {
            for x in 0..random_number(3, 10) {
                for y in 0..random_number(3, 10) {
                    let cuboid = Cuboid::new(Position::new(x, y, 0), Vector::new(1, 1, 1)).unwrap();
                    let block = Block::new(cuboid, material, BlockType::Fill);
                    surface.add_block(block);
                }
            }
        }

        // Adds flat blocks on top of valid blocks
        Self::cover_fill_blocks_with_flat_blocks(&mut surface, material);

        surface
    }

    /// Find all piles that _ends_ with a filled block and put on top a flat one
    fn cover_fill_blocks_with_flat_blocks(surface: &mut Surface, material: BlockMaterial) {
        let blocks = surface.blocks.clone();

        for block in blocks {
            let x = block.origin().x;
            let y = block.origin().y;
            // Get block at top and add a flat one on top of it
            if let Some(top) = surface.get_blocks_pile(x, y).last() {
                if top.block_type() == BlockType::Fill {
                    // Create a flat block with same width and depth
                    let flat = Block::new(
                        Cuboid::new(
                            Position::new(x, y, top.origin().z + top.volume().z),
                            Vector::new(block.volume().x, block.volume().y, 1),
                        )
                        .unwrap(),
                        material,
                        BlockType::Flat,
                    );

                    assert!(surface.add_block(flat));
                }
            }
        }
    }

    /// Remove a block at specified position and returns it (if it has been correctly removed)
    pub fn remove_at(&mut self, position: Position) -> Option<Block> {
        if let Some(index) = self
            .blocks
            .iter()
            .position(|block| block.contains(position))
        {
            Some(self.blocks.remove(index))
        } else {
            None
        }
    }

    /// Add/remove a terrain layer to the provided surface
    ///
    /// It adds/or remove a layer to the current surface within the specified X-Y range
    fn add_terrain_layer(
        surface: &mut Surface,
        min_x: Scalar,
        min_y: Scalar,
        max_x: Scalar,
        max_y: Scalar,
        n_iter: usize,
    ) {
        const MAX_HEIGHT: Scalar = 10;
        const MAX_ITER: usize = 10;

        if min_x < max_x - 1 && min_y < max_y - 1 && n_iter < MAX_ITER || surface.blocks.is_empty()
        {
            let x0 = random_number(min_x, max_x - 2);
            let x1 = random_number(x0 + 1, max_x);
            let y0 = random_number(min_y, max_y - 2);
            let y1 = random_number(y0 + 1, max_y);

            let remove = random_number(0, 20) > 14;

            for x in x0..=x1 {
                for y in y0..=y1 {
                    let z = surface
                        .get_blocks_pile(x, y)
                        .last()
                        .map_or(0, |b| b.origin().z + b.volume().z);

                    if remove {
                        // Remove blocks at layer below
                        surface.remove_at(Position::new(x, y, z - 1));
                    } else if z == 0 {
                        // This is the first layer... Select a random material
                        let material = if random_bool() {
                            BlockMaterial::Dirt
                        } else {
                            BlockMaterial::Grass
                        };
                        // Add the block
                        let cuboid =
                            Cuboid::new(Position::new(x, y, 0), Vector::new(1, 1, 1)).unwrap();
                        let block = Block::new(cuboid, material, BlockType::Fill);

                        surface.add_block(block);
                    } else {
                        // Add a block
                        // We can add dirt or grass, if at layer lower there is grass we can only add grass
                        if let Some(block) = surface.get_blocks_pile(x, y).last() {
                            if block.origin().z < MAX_HEIGHT {
                                // If the last if grass, we can only continue with grass... Dirt cannot
                                // stay on top of the grass, c'mon!
                                let material = if block.material() == BlockMaterial::Grass {
                                    BlockMaterial::Grass
                                } else if random_bool() {
                                    BlockMaterial::Dirt
                                } else {
                                    BlockMaterial::Grass
                                };
                                // Add the block
                                let cuboid =
                                    Cuboid::new(Position::new(x, y, z), Vector::new(1, 1, 1))
                                        .unwrap();
                                let block = Block::new(cuboid, material, BlockType::Fill);

                                surface.add_block(block);
                            }
                        }
                    }
                }
            }

            // Iterate to next layer if this layer was not a delete layer
            if !remove {
                for _ in 0..random_number(1, 4) {
                    Self::add_terrain_layer(surface, x0, y0, x1, y1, n_iter + 1);
                }
            }
        }
    }
}

/// Returns a iterator to iterate all blocks inside the surface
impl<'a> IntoIterator for &'a Surface {
    type Item = &'a Block;
    type IntoIter = std::slice::Iter<'a, Block>;

    /// Returns a iterator to iterate all blocks inside the surface
    fn into_iter(self) -> Self::IntoIter {
        self.blocks.iter()
    }
}

impl Default for Surface {
    /// Return an empty surface
    fn default() -> Self {
        Surface::new()
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        common::random_generators::{
            random_block, random_block_material, random_block_type, random_position,
        },
        geometry::{
            block::{Block, BlockMaterial, BlockType},
            cuboid::Cuboid,
            position::Position,
            vector::{Scalar, Vector},
        },
        test_utilities::constants::{
            NUMBER_OF_LOOPS_FOR_LITTLE_TEST, NUMBER_OF_LOOPS_FOR_NORMAL_TEST,
            NUMBER_OF_LOOPS_FOR_TINY_TEST,
        },
    };

    use super::Surface;

    #[test]
    fn add_block_test() {
        {
            // Adding twice the same block
            let block = random_block();

            let mut surface = Surface::new();

            assert!(surface.add_block(block));
            assert!(!surface.add_block(block));
        }
        {
            // Creating a cube of blocks should work
            let mut surface = Surface::new();

            const L: Scalar = 10;
            for x in 0..L {
                for y in 0..L {
                    for z in 0..L {
                        let cuboid =
                            Cuboid::new(Position::new(x, y, z), Vector { x: 1, y: 1, z: 1 })
                                .unwrap();
                        let block =
                            Block::new(cuboid, random_block_material(), random_block_type());
                        assert!(surface.add_block(block));
                        // Adding twice should fail
                        assert!(!surface.add_block(block));
                    }
                }
            }
        }
        {
            // Adding a big cuboid and then trying to add inside and outside of it
            const L: Scalar = 10;
            let mut surface = Surface::new();

            let cuboid = Cuboid::new(Position::new(0, 0, 0), Vector { x: L, y: L, z: L }).unwrap();
            let big_block = Block::new(cuboid, random_block_material(), random_block_type());

            assert!(surface.add_block(big_block));

            // Now trying adding inside
            for x in 0..L {
                for y in 0..L {
                    for z in 0..L {
                        let cuboid =
                            Cuboid::new(Position::new(x, y, z), Vector { x: 1, y: 1, z: 1 })
                                .unwrap();
                        let block =
                            Block::new(cuboid, random_block_material(), random_block_type());
                        // Block are inside the big block, this it should fail
                        assert!(!surface.add_block(block));
                    }
                }
            }

            // Now trying to adding outside
            const OFFSET: Scalar = 4;
            for x in 0 - OFFSET..=L + OFFSET {
                for y in 0 - OFFSET..=L + OFFSET {
                    for z in 0 - OFFSET..=L + OFFSET {
                        if !(0..L).contains(&x) || !(0..L).contains(&y) || !(0..L).contains(&z) {
                            let cuboid =
                                Cuboid::new(Position::new(x, y, z), Vector { x: 1, y: 1, z: 1 })
                                    .unwrap();
                            let block =
                                Block::new(cuboid, random_block_material(), random_block_type());
                            // Block are outside the big block, this it should work
                            assert!(surface.add_block(block));
                        }
                    }
                }
            }
        }
    }

    #[test]
    fn into_iter_test() {
        let mut surface = Surface::new();
        assert_eq!(0, surface.into_iter().count());

        const L: Scalar = 10;
        let mut cnt = 0;

        for x in 0..L {
            for y in 0..L {
                for z in 0..L {
                    let cuboid =
                        Cuboid::new(Position::new(x, y, z), Vector { x: 1, y: 1, z: 1 }).unwrap();
                    let block = Block::new(cuboid, random_block_material(), random_block_type());
                    assert!(surface.add_block(block));
                    cnt += 1;
                    // Check if it's returned by the iterator
                    assert!(surface.into_iter().any(|&b| b == block));
                    assert_eq!(cnt, surface.into_iter().count());
                }
            }
        }
    }

    #[test]
    fn block_at_test() {
        {
            // Creating a cube of blocks of volume 2x2x2
            let mut surface = Surface::new();

            const L: Scalar = 10;
            const BL: Scalar = 2;

            for x in (0..L).step_by(BL as usize) {
                for y in (0..L).step_by(BL as usize) {
                    for z in (0..L).step_by(BL as usize) {
                        let position = Position::new(x, y, z);
                        let cuboid = Cuboid::new(
                            position,
                            Vector {
                                x: BL,
                                y: BL,
                                z: BL,
                            },
                        )
                        .unwrap();

                        let block =
                            Block::new(cuboid, random_block_material(), random_block_type());
                        assert!(surface.add_block(block));

                        assert_eq!(Some(block), surface.block_at(position));
                    }
                }
            }

            // Now trying to adding outside
            const OFFSET: Scalar = 4;
            for x in 0 - OFFSET..=L + OFFSET {
                for y in 0 - OFFSET..=L + OFFSET {
                    for z in 0 - OFFSET..=L + OFFSET {
                        if !(0..L).contains(&x) || !(0..L).contains(&y) || !(0..L).contains(&z) {
                            let position = Position::new(x, y, z);
                            assert!(surface.block_at(position).is_none());
                        }
                    }
                }
            }
        }
    }

    #[test]
    fn volume_test() {
        {
            // Empty surface
            let surface = Surface::new();
            assert!(surface.volume().is_none());
        }
        {
            // One block surface
            let mut surface = Surface::new();
            let block = random_block();
            surface.add_block(block);
            assert_eq!(block.cuboid(), surface.volume().unwrap());
        }
        {
            // Block along diagonals that getting bigger than bigger
            let mut surface = Surface::new();
            let mut p = 1;
            for i in 1..1000 {
                let cuboid = Cuboid::new(Position::new(p, p, p), Vector::new(i, i, i)).unwrap();
                p += i;
                let block = Block::new(cuboid, BlockMaterial::Grass, BlockType::Fill);
                assert!(surface.add_block(block));
                let q = (i as f32 / 2.0 * (i + 1) as f32).ceil() as i32;

                assert_eq!(Vector::new(q, q, q), surface.volume().unwrap().volume());

                assert_eq!(Position::new(1, 1, 1), surface.volume().unwrap().origin());
            }
        }
    }

    #[test]
    fn pile_test() {
        {
            // Empty surface
            let surface = Surface::new();
            for x in 0..NUMBER_OF_LOOPS_FOR_LITTLE_TEST as Scalar {
                for y in 0..NUMBER_OF_LOOPS_FOR_LITTLE_TEST as Scalar {
                    assert_eq!(0, surface.get_blocks_pile(x, y).count());
                }
            }
        }
        {
            // One block surface
            let mut surface = Surface::new();
            let block = random_block();
            surface.add_block(block);
            let x = block.origin().x;
            let y = block.origin().y;

            assert_eq!(1, surface.get_blocks_pile(x, y).count());
            assert_eq!(&block, surface.get_blocks_pile(x, y).next().unwrap());
        }
        {
            // Semi-pyramid surface
            let mut surface = Surface::new();

            for z in 0..NUMBER_OF_LOOPS_FOR_TINY_TEST as Scalar {
                for x in z..NUMBER_OF_LOOPS_FOR_TINY_TEST as Scalar {
                    for y in z..NUMBER_OF_LOOPS_FOR_TINY_TEST as Scalar {
                        let cuboid =
                            Cuboid::new(Position::new(x, y, z), Vector::new(1, 1, 1)).unwrap();
                        let block = Block::new(cuboid, BlockMaterial::Grass, BlockType::Fill);
                        assert!(surface.add_block(block));
                    }
                }
            }

            for x in 0..NUMBER_OF_LOOPS_FOR_TINY_TEST as Scalar {
                for y in 0..NUMBER_OF_LOOPS_FOR_TINY_TEST as Scalar {
                    let z = (x + 1).min(y + 1) as usize;
                    assert_eq!(z, surface.get_blocks_pile(x, y).count());

                    for (i, b) in surface.get_blocks_pile(x, y).enumerate() {
                        assert_eq!(x, b.origin().x);
                        assert_eq!(y, b.origin().y);
                        assert_eq!(i, b.origin().z as usize);
                    }
                }
            }
        }
    }

    #[test]
    fn cover_fill_blocks_with_flat_blocks_test() {
        {
            // Semi-pyramid surface
            let mut surface = Surface::new();

            for z in (0..NUMBER_OF_LOOPS_FOR_TINY_TEST as Scalar).step_by(2) {
                for x in (z..NUMBER_OF_LOOPS_FOR_TINY_TEST as Scalar).step_by(2) {
                    for y in (z..NUMBER_OF_LOOPS_FOR_TINY_TEST as Scalar).step_by(2) {
                        let cuboid =
                            Cuboid::new(Position::new(x, y, z), Vector::new(2, 2, 2)).unwrap();
                        let block = Block::new(cuboid, BlockMaterial::Grass, BlockType::Fill);
                        assert!(surface.add_block(block));
                    }
                }
            }

            Surface::cover_fill_blocks_with_flat_blocks(&mut surface, BlockMaterial::Grass);

            for x in (0..NUMBER_OF_LOOPS_FOR_TINY_TEST as Scalar).step_by(2) {
                for y in (0..NUMBER_OF_LOOPS_FOR_TINY_TEST as Scalar).step_by(2) {
                    let z = (x / 2).min(y / 2) + 1;
                    assert_eq!(
                        z + 1,
                        surface.get_blocks_pile(x, y).count() as Scalar,
                        "x {}, y {}",
                        x,
                        y
                    );

                    let flat = surface.get_blocks_pile(x, y).last().unwrap();
                    {
                        assert_eq!(x, flat.origin().x);
                        assert_eq!(y, flat.origin().y);
                        assert_eq!(z * 2, flat.origin().z, "x {} y {}", x, y);
                        assert_eq!(2, flat.volume().x);
                        assert_eq!(2, flat.volume().y);
                        assert_eq!(1, flat.volume().z);
                        assert_eq!(BlockType::Flat, flat.block_type());
                        assert_eq!(BlockMaterial::Grass, flat.material());
                    }
                }
            }
        }
    }

    #[test]
    fn remove_at_test() {
        {
            // Empty surface
            let mut surface = Surface::new();

            for _ in 0..NUMBER_OF_LOOPS_FOR_NORMAL_TEST {
                let position = random_position();
                assert!(surface.remove_at(position).is_none());
            }
        }
        {
            // Half cube
            let mut surface = Surface::new();

            for x in 0..NUMBER_OF_LOOPS_FOR_TINY_TEST as Scalar {
                for y in 0..NUMBER_OF_LOOPS_FOR_TINY_TEST as Scalar {
                    for z in 0..=x + y {
                        let cuboid =
                            Cuboid::new(Position::new(x, y, z), Vector::new(1, 1, 1)).unwrap();
                        let block = Block::new(cuboid, BlockMaterial::Grass, BlockType::Fill);
                        assert!(surface.add_block(block));
                    }
                }
            }

            for x in 0..NUMBER_OF_LOOPS_FOR_TINY_TEST as Scalar {
                for y in 0..NUMBER_OF_LOOPS_FOR_TINY_TEST as Scalar {
                    for z in 0..NUMBER_OF_LOOPS_FOR_TINY_TEST as Scalar {
                        let p = Position::new(x, y, z);
                        let b = surface.remove_at(p);

                        if z <= x + y {
                            assert!(b.is_some());
                            assert_eq!(Position::new(x, y, z), b.unwrap().origin());
                        } else {
                            assert!(b.is_none());
                        }
                    }
                }
            }
        }
    }
}
