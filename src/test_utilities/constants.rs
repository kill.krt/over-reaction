#![cfg(test)]
#![allow(dead_code)]

pub const NUMBER_OF_LOOPS_FOR_TINY_TEST: usize = 20;
pub const NUMBER_OF_LOOPS_FOR_LITTLE_TEST: usize = 100;
pub const NUMBER_OF_LOOPS_FOR_SMALL_TEST: usize = 5_000;
pub const NUMBER_OF_LOOPS_FOR_NORMAL_TEST: usize = 16_000;
pub const NUMBER_OF_LOOPS_FOR_MID_TEST: usize = 256_000;
pub const NUMBER_OF_LOOPS_FOR_BIG_TEST: usize = 1_000_000;
pub const NUMBER_OF_LOOPS_FOR_HUGE_TEST: usize = 5_000_000;
