#![allow(dead_code)]

use common::random_generators::random_number;
use geometry::block::Block;
use geometry::block::BlockMaterial;
use geometry::cuboid::Cuboid;
use geometry::position::Position;
use geometry::surface::Surface;
use geometry::vector::Scalar;
use geometry::vector::Vector;
use rocket::fs::relative;
use rocket::fs::NamedFile;
use rocket::serde::json::Json;

use std::net::IpAddr;
use std::path::{Path, PathBuf};

#[macro_use]
extern crate rocket;

mod common;
mod geometry;

#[cfg(test)]
mod test_utilities;

/// Generate a vector with random coordinates generate in the range of (min, max)
fn create_random_vector(min: i32, max: i32) -> Vector {
    let x = random_number(min, max);
    let y = random_number(min, max);
    let z = random_number(min, max);

    Vector::new(x, y, z)
}

// Get resources inside dist folder
#[rocket::get("/<path..>")]
pub async fn dist_files(path: PathBuf) -> Option<NamedFile> {
    let mut path = Path::new(relative!("dist")).join(path);

    if path.is_dir() {
        // If a folder is requested, just return the index.html in that folder
        path.push("index.html");
    }

    NamedFile::open(path).await.ok()
}

#[rocket::get("/data", format = "json")]
pub fn get_data() -> Json<Surface> {
    let surface = Surface::generate_random_terrain(25, 25, BlockMaterial::Grass);

    Json(surface)
}

fn random_surface(
    surface: &mut Surface,
    min_x: Scalar,
    max_x: Scalar,
    min_y: Scalar,
    max_y: Scalar,
    z: Scalar,
) {
    let ox = min_x;
    let ex = random_number(ox + 1, max_x);
    let oy = min_y;
    let ey = random_number(oy + 1, max_y);

    for x in ox..=ex {
        for y in oy..=ey {
            let cuboid = Cuboid::new(Position::new(x, y, z), Vector { x: 1, y: 1, z: 1 }).unwrap();
            let block = Block::new(
                cuboid,
                geometry::block::BlockMaterial::Sand,
                geometry::block::BlockType::Flat,
            );
            surface.add_block(block);
        }
    }

    if random_number(0, 10) > 3 && z < 6 {
        random_surface(surface, ox, ex, oy, ey, z + 1);
    }
}

#[launch]
fn rocket() -> _ {
    // Get IP address of this machine
    let local_ip: IpAddr = local_ipaddress::get().unwrap().parse().ok().unwrap();
    // Configure Rocket with correct IP address
    let figment = rocket::Config {
        address: local_ip,
        ..rocket::Config::default()
    };
    // Launch Rocket
    rocket::custom(figment)
        .mount("/", rocket::routes![dist_files])
        .mount("/api", routes![get_data])
}
