#![allow(dead_code)]

use num_traits::Num;

use crate::geometry::{
    block::{Block, BlockMaterial, BlockType},
    cuboid::Cuboid,
    position::Position,
    vector::{Scalar, Vector},
};

/// Generate a random number, max is included
pub fn random_number<T>(min: T, max: T) -> T
where
    T: Num + std::cmp::PartialOrd + rand::distributions::uniform::SampleUniform,
{
    use rand::Rng;
    let mut rng = rand::thread_rng();
    // Generate a random number
    rng.gen_range(min..=max)
}

/// Generate a vector with random coordinates generate in the range of (min, max)
pub fn random_vector(min: Scalar, max: Scalar) -> Vector {
    let x = random_number(min as Scalar, max as Scalar);
    let y = random_number(min as Scalar, max as Scalar);
    let z = random_number(min as Scalar, max as Scalar);

    Vector::new(x, y, z)
}

/// Generate a position with random coordinates generate in the range of (-100, 100)
pub fn random_position() -> Position {
    let min = -100;
    let max = 100;

    Position::from(random_vector(min, max))
}

/// Generate a random string with given len, 0 means random len [0 - 100)
pub fn random_string(len: Scalar) -> String {
    use rand::distributions::Alphanumeric;
    use rand::{thread_rng, Rng};

    let length = if len == 0 { random_number(0, 100) } else { len };

    let text: String = std::iter::repeat(())
        .map(|()| thread_rng().sample(Alphanumeric) as char)
        .take(length as usize)
        .collect();

    text
}

/// Generate a random boolean value
pub fn random_bool() -> bool {
    random_number(0, 10) % 2 == 0
}

/// Generate a random cuboid
pub fn random_cuboid() -> Cuboid {
    let size = random_vector(1, 15);
    let origin = random_position();

    Cuboid::new(origin, size).unwrap()
}

pub fn random_block_type() -> BlockType {
    match random_number(0, 1) {
        0 => BlockType::Fill,
        1 => BlockType::Flat,
        _ => panic!(),
    }
}

pub fn random_block_material() -> BlockMaterial {
    match random_number(0, 5) {
        0 => BlockMaterial::Grass,
        1 => BlockMaterial::Ice,
        2 => BlockMaterial::Rock,
        3 => BlockMaterial::Sand,
        4 => BlockMaterial::Water,
        5 => BlockMaterial::Dirt,
        _ => panic!(),
    }
}

/// Generate a random block
pub fn random_block() -> Block {
    let material = random_block_material();
    let block_type = random_block_type();
    let cuboid = random_cuboid();

    Block::new(cuboid, material, block_type)
}
